//
//  Machines.swift
//  MVP_Sample
//
//  Created by Tulio Bazan on 20/09/16.
//  Copyright © 2016 Tulio. All rights reserved.
//

import Foundation

protocol Machine {
    var baseURL : String { get }
}

enum Machines : Machine {
    case sandbox
    
    var baseURL : String {
        get {
            switch self {
            case .sandbox:
                return "http://localhost:8080/api"
            }
        }
    }
}