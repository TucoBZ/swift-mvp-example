//
//  APIManager.swift
//  MVP_Sample
//
//  Created by Tulio Bazan on 20/09/16.
//  Copyright © 2016 Tulio. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import PromiseKit

struct APIManager {
    var machine: Machine = Machines.sandbox
    var timeout: NSTimeInterval = 2.0
    
    static let sharedInstance = APIManager()
    
    func requestObject<T: Mappable>(_: T.Type, request: APIRequest, callback:  ((Response<T, NSError>) -> Void)) {
        let urlRequest = createURLRequest(request)
        Alamofire.request(urlRequest).responseObject(completionHandler: callback)
    }
    
    func requestArrayObject<T: Mappable>(_: T.Type, request: APIRequest, callback:  ((Response<[T], NSError>) -> Void)) {
        let urlRequest = createURLRequest(request)
        Alamofire.request(urlRequest).validate().responseArray(completionHandler: callback)
    }
    
    func createURLRequest(request: APIRequest) -> NSMutableURLRequest {
        
        guard let URL = NSURL(string: machine.baseURL) else { return NSMutableURLRequest() }
        let URLResquest = NSURLRequest(URL: URL.URLByAppendingPathComponent(request.urlEndPoint))
        let mutableRequest = request.encoding.encode(URLResquest, parameters: request.parameters).0
        mutableRequest.HTTPMethod = request.method.rawValue
        mutableRequest.timeoutInterval = timeout
        mutableRequest.allHTTPHeaderFields?["Authorization"] = ""
        
        return mutableRequest
    }
}

