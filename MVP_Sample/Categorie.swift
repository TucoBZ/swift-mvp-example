//
//  Categorie.swift
//  MVP_Sample
//
//  Created by Tulio Bazan on 22/09/16.
//  Copyright © 2016 Tulio. All rights reserved.
//

import Foundation
import ObjectMapper

struct Categorie {
    var id: Int?
    var defaultParentCategory: String?
    var type: String?
    var displayName: String?
    var description: String?
    
    init?(_ map: Map) {}
}

extension Categorie: Mappable {
    mutating func mapping(map: Map) {
        id                      <- map["id"]
        defaultParentCategory   <- map["defaultParentCategory"]
        type                    <- map["type"]
        displayName             <- map["displayName"]
        description             <- map["description"]
    }
}
