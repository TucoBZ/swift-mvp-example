//
//  CategoriesViewController.swift
//  MVP_Sample
//
//  Created by Tulio Bazan on 20/09/16.
//  Copyright © 2016 Tulio. All rights reserved.
//

import UIKit

protocol CategoriesView: class {
    func updateDataSource()
    func setRefreshControl(refreshControl: UIRefreshControl)
}

class CategoriesViewController: UIViewController {
    
    var presenter: CategoriesPresenter?
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = CategoriesPresenter.init(view: self)
        tableView.dataSource = presenter
        tableView.delegate = presenter
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CategoriesViewController : CategoriesView {
    
    func updateDataSource() {
        tableView.reloadData()
    }
    
    func setRefreshControl(refreshControl: UIRefreshControl){
        tableView.addSubview(refreshControl)
    }
}



