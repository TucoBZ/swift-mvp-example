//
//  RootCategories.swift
//  MVP_Sample
//
//  Created by Tulio Bazan on 21/09/16.
//  Copyright © 2016 Tulio. All rights reserved.
//

import Foundation
import ObjectMapper

struct RootCategories {
    var rootCategories: [Categorie]
    
    init?(_ map: Map) {
        rootCategories = []
    }
}

extension RootCategories: Mappable {
    mutating func mapping(map: Map) {
        rootCategories          <- map["rootCategories"]
    }
}

