//
//  Request.swift
//  MVP_Sample
//
//  Created by Tulio Bazan on 20/09/16.
//  Copyright © 2016 Tulio. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

protocol APIRequest {
    var urlEndPoint : String { get }
    var method : Alamofire.Method { get }
    var encoding: Alamofire.ParameterEncoding { get }
    var parameters: [String : AnyObject]? { get }
}


enum Requests : APIRequest {
    case categorias(parameters: [String : AnyObject]?)
    
    var urlEndPoint : String {
        get {
            switch self {
            case .categorias:
                return "/catalog/products/categories"
            }
        }
    }
    
    var method : Alamofire.Method {
        get {
            switch self {
            case .categorias:
                return .GET
            }

        }
    }
    
    var encoding : Alamofire.ParameterEncoding {
        get {
            switch self {
            default:
                return .JSON
            }
        }
    }
    
    var parameters: [String : AnyObject]? {
        get {
            switch self {
            case .categorias(let param):
                return param
            default:
                return nil
            }
        }
    }
}