//
//  CategoriesPresenter.swift
//  MVP_Sample
//
//  Created by Tulio Bazan on 21/09/16.
//  Copyright © 2016 Tulio. All rights reserved.
//

import UIKit

protocol CategoriesViewPresenter: PresenterDefault {
    init(view: CategoriesView)
    func startRequest()
}

class CategoriesPresenter : NSObject, CategoriesViewPresenter {
    unowned let view: CategoriesView
    var refreshControl: UIRefreshControl?
    var rootCategories: RootCategories? {
        didSet {
            refreshControl?.endRefreshing()
        }
    }
    
    required init(view: CategoriesView) {
        self.view = view
        super.init()
        
        refreshControl = UIRefreshControl()
        guard let refreshControl = refreshControl else {return}
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(CategoriesPresenter.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.view.setRefreshControl(refreshControl)
        
        defaultPresentation()
    }
    
    func startRequest() {
        let myRequest = Requests.categorias(parameters: nil)
        APIManager.sharedInstance.requestObject(RootCategories.self, request: myRequest) { [weak self] response in
            print(response.data)
            print(response.result)
            print(response.result.error)
            print(response.result.value)
            self?.rootCategories = response.result.value
            self?.view.updateDataSource()
        }
    }
    
    func defaultPresentation() {
        startRequest()
    }
    
    func refresh(sender:AnyObject) {
        startRequest()
    }
}

extension CategoriesPresenter:  UITableViewDataSource {
    
    @objc func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    @objc func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rootCategories?.rootCategories.count ?? 0
    }
    
    @objc func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let categorie =  rootCategories?.rootCategories[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("categorieCell", forIndexPath: indexPath)
        cell.textLabel?.text = categorie?.displayName
        return cell
    }
}

extension CategoriesPresenter : UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //Vai para a proxima tela
        
    }
}
