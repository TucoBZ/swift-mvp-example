//
//  PresenterDefault.swift
//  MVP_Sample
//
//  Created by Tulio Bazan on 21/09/16.
//  Copyright © 2016 Tulio. All rights reserved.
//

import Foundation

protocol PresenterDefault {
    func defaultPresentation()
}
