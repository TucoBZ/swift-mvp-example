//
//  APIManager_Tests.swift
//  MVP_Sample
//
//  Created by Tulio Bazan on 22/09/16.
//  Copyright © 2016 Tulio. All rights reserved.
//

import XCTest
@testable import MVP_Sample

class APIManager_Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCallback() {
        var rootCategorie : RootCategories?
        
        let asyncExpectation = expectationWithDescription("longRunningFunction")

        let myRequest = Requests.categorias(parameters: nil)
        APIManager.sharedInstance.requestObject(RootCategories.self, request: myRequest) { response in
            XCTAssertNil(response.result.error, "Opa... Tem Erro!")
            rootCategorie = response.result.value
            XCTAssertNotNil(rootCategorie, "eita")
            asyncExpectation.fulfill()
        }
      
        waitForExpectationsWithTimeout(APIManager.sharedInstance.timeout) { error in
            XCTAssertNil(error, "Opa... Tem Erro!")
        }
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
